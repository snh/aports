# Maintainer: psykose <alice@ayaya.dev>
pkgname=scsi-tgt
pkgver=1.0.86
pkgrel=5
pkgdesc="user-space iSCSI target daemon"
url="https://github.com/fujita/tgt"
arch="all"
license="GPL-2.0-only"
makedepends="
	docbook-xsl
	libxslt
	linux-headers
	"
subpackages="$pkgname-openrc $pkgname-doc $pkgname-scripts::noarch"
source="$pkgname-$pkgver.tar.gz::https://github.com/fujita/tgt/archive/refs/tags/v$pkgver.tar.gz
	ldflags.patch
	lfs64.patch
	posix-fallocate.patch
	reg.patch
	tgt-admin.confd
	tgt-admin.initd
	tgtd.confd
	tgtd.initd
	"
builddir="$srcdir/tgt-$pkgver"
options="!check" # no tests

build() {
	make LDFLAGS="$LDFLAGS"
}

package() {
	make DESTDIR="$pkgdir" install

	install -Dm755 "$srcdir"/tgtd.initd \
		"$pkgdir"/etc/init.d/tgtd
	install -Dm644 "$srcdir"/tgtd.confd \
		"$pkgdir"/etc/conf.d/tgtd
	install -Dm755 "$srcdir"/tgt-admin.initd \
		"$pkgdir"/etc/init.d/tgt-admin
	install -Dm644 "$srcdir"/tgt-admin.confd \
		"$pkgdir"/etc/conf.d/tgt-admin
}

doc() {
	default_doc
	amove etc/tgt/examples
}

scripts() {
	pkgdesc="$pkgdesc (perl/bash scripts)"
	depends="$pkgname=$pkgver-r$pkgrel bash perl perl-config-general"

	amove \
		usr/sbin/tgt-admin \
		usr/sbin/tgt-setup-lun
}

sha512sums="
f4e8e87691be2793c5eee9d7dc744ae3a7b47f710f559141f0533de345f727fb68223a3090282f5c51f70ea55fbf5936d81e3b08ce953b7ac48013295c32bef7  scsi-tgt-1.0.86.tar.gz
84a3ae20cff6e265dfbb7ca47390303ac6caed38f1112ed0227fb9742903395725e269bcc4b6ebd08db710301bc49236e95d5d242abb2a5f9ba94335aae6b7d6  ldflags.patch
b2dd123662de37bef39db6c5d6282ec188bad36ce3121c50a9c94582c3126cfd59ed0400158d66505296218477fce7a15185cc889c2c9f35f8439332fafcdeef  lfs64.patch
3a7aba33e76305b339e472ae99b12d7e00478996b4c420950143d0ebf71664fe41260f51cb14096bf042817ff39e34a9acc7af5956f811de892e3133f1ef321a  posix-fallocate.patch
361233a15475988dd65eae7d7eaf53b94c05630f2c5ad80923e57063a8ef92c4c7630f4aef6bdadbd2ab0a7c779a3e196a400c7ec397a54c61bc19963dfda422  reg.patch
1eac80c05c462a325582aa610f5f37f427eb70695bd95b66fe368fbbee24455b7cb80162a2925919544253703a83fd6a96f1d1a383ddf3889b44d82904fcae50  tgt-admin.confd
69b3348d2144c33830fea38c96c5f8b210fb1fe49b24a1f6c92f459740d41f014dea891887693e8a8b1ea723d1f78ee2a5fd74887fc27b683a4b98fd93a0ebe9  tgt-admin.initd
5f44e86da54aede9c2b02da830a1e9c870e30eef83130cd4fd18976eefb52541b54ef22ad93814b218a9d5a8203869246307b60ada98318c587c27f41dbf9ae5  tgtd.confd
259efbb9259d05b97b45d418901af1e08cb2c2e390cef5fff508666e5967d9ee36a56795a9e2029826f38663d6608909dce65c7e2eda07a9d5e5e3ee17580952  tgtd.initd
"
