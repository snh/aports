#!/sbin/openrc-run

name="tgt-admin"
extra_started_commands="reload forcereload"

: ${tgtd_config:=/etc/tgt/targets.conf}
required_files="$tgtd_config"

depend() {
	need tgtd
}

start() {
	ebegin "Setting tgtd in 'offline' state for configuration"
	# Put tgtd into "offline" state until all the targets are configured.
	tgtadm --op update --mode sys --name State -v offline
	eend $?

	ebegin "Configuring targets"
	tgt-admin -e -c $tgtd_config
	eend $?

	ebegin "Setting tgtd into 'ready' state"
	tgtadm --op update --mode sys --name State -v ready
	eend $?
}

stop() {
	ebegin "Setting all targets offline before stopping"
	tgtadm --op update --mode sys --name State -v offline
	tgt-admin --offline ALL
	eend $?

	ebegin "Forcefully clearing all targets before shutting down tgtd"
	tgt-admin --update ALL -c /dev/null -f
	eend $?
}

reload() {
	ebegin "Reloading tgt target configuration"
	tgt-admin --update ALL -c "$tgtd_config"
	eend $?
}

forcereload() {
	ebegin "Force-updating tgt target configuration"
	tgt-admin --update ALL -f -c "$tgtd_config"
	eend $?
}
