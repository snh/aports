# Contributor: Guy Godfroy <guy.godfroy@gugod.fr>
# Maintainer: psykose <alice@ayaya.deV>
pkgname=py3-xsdata
pkgver=23.7
pkgrel=0
pkgdesc="Naive XML & JSON Bindings for python"
url="https://github.com/tefra/xsdata"
arch="noarch"
license="MIT"
# cli feature deps
depends="
	py3-click
	py3-click-default-group
	py3-docformatter
	py3-jinja2
	py3-toposort
	"
makedepends="
	py3-gpep517
	py3-setuptools
	py3-wheel
	"
checkdepends="
	py3-lxml
	py3-pytest-benchmark
	py3-pytest-forked
	py3-requests
	"
subpackages="$pkgname-pyc"
source="$pkgname-$pkgver.tar.gz::https://github.com/tefra/xsdata/archive/refs/tags/v$pkgver.tar.gz
	typing.patch
	"
builddir="$srcdir/xsdata-$pkgver"

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages .testenv
	.testenv/bin/python3 -m installer .dist/*.whl
	.testenv/bin/python3 -m pytest --forked
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/*.whl
	# rm -r "$pkgdir"/usr/lib/python*/site-packages/bite/tests
}

sha512sums="
84b24f0a00414a5c9ce818eff6de7cde3bd1eefd7de6b78dc9067d0106a59e64edd60382f6b384ab27916e7b07578556939e300fc07632ce43536d275b8f3618  py3-xsdata-23.7.tar.gz
06bdf559a031b986b14647095f35d2147a6f56a248c6d826b03e38ef78ff712969f5575a01569ec7e02587481c67f1a0372118693a8777ac22b75e0cc787efcd  typing.patch
"
