# Contributor: Orhun Parmaksız <orhunparmaksiz@gmail.com>
# Maintainer: Orhun Parmaksız <orhunparmaksiz@gmail.com>
pkgname=tere
pkgver=1.5.0
pkgrel=1
pkgdesc="Terminal file explorer"
url="https://github.com/mgunyho/tere"
arch="all"
license="EUPL-1.2"
makedepends="
	cargo
	cargo-auditable
	"
subpackages="
	$pkgname-doc
	"
source="$pkgname-$pkgver.tar.gz::https://github.com/mgunyho/tere/archive/v$pkgver.tar.gz
	deps.patch
	"
options="net"

prepare() {
	default_prepare

	cargo fetch --target="$CTARGET" --locked
}

build() {
	cargo auditable build --release --frozen
}

check() {
	# https://github.com/mgunyho/tere/issues/90
	cargo test --frozen -- --skip "test_case_sensitive_mode_change" --skip "panic_guard"
}

package() {
	install -Dm 755 target/release/tere -t "$pkgdir"/usr/bin
	install -Dm 644 README.md -t "$pkgdir"/usr/share/doc/$pkgname
	install -Dm 644 ./demo/* -t "$pkgdir"/usr/share/dox/$pkgname/demo
	install -Dm 644 LICENSE -t "$pkgdir"/usr/share/licenses/$pkgname
}

sha512sums="
7c60e3fa08ef877955ec6faaee351dc2571f1ddf6ad953154559fd7da3006a3e62a1bb289a6631b04168ecf97768662a2af996082e0762e129c7a440ef8d66b9  tere-1.5.0.tar.gz
8884a5a025de337bb300917531413b40ff79ed796821570f902247ee146fc0cd025f9090cc75005e07747fcb900988cc9a3105d6e9502f5068c3718f96a24425  deps.patch
"
