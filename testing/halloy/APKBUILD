# Contributor: Celeste <cielesti@protonmail.com>
# Maintainer: Celeste <cielesti@protonmail.com>
pkgname=halloy
pkgver=2023.2
pkgrel=0
pkgdesc="Rust graphical IRC client supporting IRCv3.2 capabilities"
url="https://github.com/squidowl/halloy"
# s390x: nix crate
arch="all !s390x"
license="GPL-3.0-or-later"
makedepends="
	cargo
	cargo-auditable
	openssl-dev
	"
source="https://github.com/squidowl/halloy/archive/$pkgver/halloy-$pkgver.tar.gz"

prepare() {
	default_prepare

	# Fix 'undefined reference to "open64"'
	cargo update -p getrandom@0.2.9 --precise 0.2.10
	# Fix x86 issue: https://github.com/starkat99/half-rs/issues/93
	cargo update -p half --precise 2.3.1

	cargo fetch --target="$CTARGET" --locked
}

build() {
	cargo auditable build --frozen --release
}

check() {
	cargo test --frozen
}

package() {
	local appid="org.squidowl.halloy"

	install -Dm755 target/release/halloy -t "$pkgdir"/usr/bin

	install -Dm644 assets/halloy.desktop \
		"$pkgdir"/usr/share/applications/$appid.desktop
	install -Dm644 assets/halloy.appdata.xml \
		"$pkgdir"/usr/share/metainfo/$appid.appdata.xml
	install -Dm644 assets/logo_128px.png \
		"$pkgdir"/usr/share/icons/hicolor/128x128/apps/$appid.png
}

sha512sums="
a5f76b7ea4713967f49eeb7ed2b2093f05d53c1686144427adef141c8437494a4d609552a1ff54fbbfe9e569175af74e91cf724ca98d378c99e548338c20c7a9  halloy-2023.2.tar.gz
"
