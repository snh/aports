# Contributor: Francesco Colista <fcolista@alpinelinux.org>
# Maintainer: Francesco Colista <fcolista@alpinelinux.org>
pkgname=py3-sip
pkgver=6.7.10
pkgrel=0
pkgdesc="A tool that makes it easy to create Python bindings for C and C++ libraries"
options="!check" # No testsuite
url="http://www.riverbankcomputing.com/software/sip/"
arch="all"
license="custom:sip"
depends="
	py3-packaging
	py3-ply
	py3-setuptools
	py3-toml
	"
makedepends="python3-dev py3-gpep517 py3-wheel"
subpackages="$pkgname-pyc"
source="https://pypi.python.org/packages/source/s/sip/sip-$pkgver.tar.gz"
builddir="$srcdir/sip-$pkgver"

replaces="py-sip" # Backwards comptibility
provides="py-sip=$pkgver-r$pkgrel" # Backwards comptibility

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/sip-*.whl
}

sha512sums="
56f18fe6599dc74a90c07c58a7f1c23511c7f3661a3ec507d70fd2d32e636713ecd3cf1c960a3687261175c9a42df9de099e28cd1e6c0067ed755b97fc753e96  sip-6.7.10.tar.gz
"
