# Contributor: Jakub Jirutka <jakub@jirutka.cz>
# Maintainer: Jakub Jirutka <jakub@jirutka.cz>
pkgname=nghttp3
pkgver=0.13.0
pkgrel=0
pkgdesc="HTTP/3 library written in C"
url="https://github.com/ngtcp2/nghttp3"
arch="all"
license="MIT"
makedepends="cmake samurai"
checkdepends="cunit-dev"
subpackages="$pkgname-dev"
source="https://github.com/ngtcp2/nghttp3/releases/download/v$pkgver/nghttp3-$pkgver.tar.gz"

build() {
	local crossopts=
	if [ "$CBUILD" != "$CHOST" ]; then
		crossopts="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake -G Ninja -B build \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		$crossopts .
	cmake --build build
}

check() {
	cmake --build build --target check
}

package() {
	DESTDIR="$pkgdir" cmake --install build

	# Contains just README.rst.
	rm -rf "$pkgdir"/usr/share/doc
}

sha512sums="
823a4c22af8e36b2d782319e1e411a10d7e119f5ded3768537bfedd82a41657b2de219d3b94a2b54ea786acbf85d40b56e34fc7a74cc2b8ca7cb4bbb4fca798d  nghttp3-0.13.0.tar.gz
"
