# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-plasma
pkgname=plasma-pa
pkgver=5.27.6
pkgrel=0
pkgdesc="Plasma applet for audio volume management using PulseAudio"
# armhf blocked by qt5-qtdeclarative
arch="all !armhf"
url="https://kde.org/plasma-desktop/"
license="LGPL-2.1-only OR LGPL-3.0-only AND GPL-2.0-only"
depends="pulseaudio kirigami2"
makedepends="
	extra-cmake-modules
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	kcoreaddons-dev
	kdeclarative-dev
	kdoctools-dev
	kglobalaccel-dev
	knotifications-dev
	ki18n-dev
	plasma-workspace-dev
	pulseaudio-dev
	libcanberra-dev
	samurai
	"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
source="https://download.kde.org/$_rel/plasma/$pkgver/plasma-pa-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"

_commit=""
snapshot() {
	clean
	makedepends="git xz rsync tar" deps
	mkdir -p "$srcdir" && cd "$srcdir"
	git clone --filter=tree:0 https://invent.kde.org/plasma/plasma-pa.git .
	git archive --format tar --prefix=$pkgname/ $_commit > ../$pkgname-$_commit.tar
	xz -vv -T0 -9 -e ../$pkgname-$_commit.tar
}

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DUSE_GCONF=OFF
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
2d1633e96b105e7aecfd05dfe17b0dabd28e6d1f4103575fa7a006868e7d6083954ed3176bd9c5c2c0d7f0e102f91bb5296eaf8a7de5ab8baad63d8f6a23e84b  plasma-pa-5.27.6.tar.xz
"
