# Contributor: Leo <thinkabit.ukim@gmail.com>
# Maintainer: Michał Polański <michal@polanski.me>
pkgname=py3-tomlkit
_realname=tomlkit
pkgver=0.12.0
pkgrel=0
pkgdesc="Style-preserving TOML library for Python"
url="https://github.com/sdispater/tomlkit"
arch="noarch"
license="MIT"
makedepends="py3-gpep517 py3-installer py3-poetry-core"
checkdepends="py3-pytest py3-yaml"
subpackages="$pkgname-pyc"
source="https://files.pythonhosted.org/packages/source/${_realname:0:1}/$_realname/$_realname-$pkgver.tar.gz"
builddir="$srcdir/$_realname-$pkgver"

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	pytest
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/tomlkit-$pkgver-py3-none-any.whl
}

sha512sums="
821c2a3d2e5b8618f67b66c73c13ece909cedf320a717d92e8d9d0ded6a59a681354f1345c2854d452fdfc7cce97c316d9a2ae9f95aa500a99f6ab553f4eb1fe  tomlkit-0.12.0.tar.gz
"
