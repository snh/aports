# Maintainer: Michał Polański <michal@polanski.me>
pkgname=committed
pkgver=1.0.18
pkgrel=2
pkgdesc="Nitpicking your commit history"
url="https://github.com/crate-ci/committed"
license="MIT OR Apache-2.0"
arch="all"
makedepends="cargo libgit2-dev cargo-auditable"
source="https://github.com/crate-ci/committed/archive/v$pkgver/committed-$pkgver.tar.gz
	libgit2.patch
	"
options="net" # fetch dependencies

prepare() {
	default_prepare

	# Rust target triple.
	local target=$(rustc -vV | sed -n 's/host: //p')

	# Build against system-provided libs
	mkdir -p .cargo
	cat >> .cargo/config.toml <<-EOF
		[target.$target]
		git2 = { rustc-link-lib = ["git2"] }
	EOF

	# open64
	cargo update -p getrandom --precise 0.2.10

	cargo fetch --target="$CTARGET" --locked
}

build() {
	cargo auditable build --frozen --release
}

check() {
	cargo test --frozen
}

package() {
	install -Dm755 target/release/$pkgname -t "$pkgdir"/usr/bin/
}

sha512sums="
79a441308ca763f3db768ef0f7a2df7eade79a90d769f06a16c75be9b9bda24762879d21cbb29ea59afce8700b197cdaf83beefb5fdc4cd15a7c0791d2bc2ea7  committed-1.0.18.tar.gz
58948cf7195c659d9c244c93b8f01a0c7e9e37053b4e7f0678d23722c5c9706b69961d6c0e6ab22209e20db98ec7d7b4d294aa8d85b0d031105eeecb0bcdedda  libgit2.patch
"
